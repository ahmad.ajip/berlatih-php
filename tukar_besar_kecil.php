<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
<?php
// no 3
function tukar_besar_kecil($string){
    $result = "";
    for($i=0; $i<strlen($string); $i+=1){
        $ascii_char = ord($string[$i]);
        if($ascii_char == 32){
            $result .= $string[$i];
        }
        else if ($ascii_char >= 65 && $ascii_char <=90){
            $result .= chr($ascii_char + 32);
        }
        else if ($ascii_char >= 97 && $ascii_char <=122){
            $result .= chr($ascii_char - 32);
        }
        else{
            $result .= $string[$i];
        }
    }
    echo ($result)."<br>";
}

// TEST CASES
echo tukar_besar_kecil('Hello World'); // "hELLO wORLD"
echo tukar_besar_kecil('I aM aLAY'); // "i Am Alay"
echo tukar_besar_kecil('My Name is Bond!!'); // "mY nAME IS bOND!!"
echo tukar_besar_kecil('IT sHOULD bE me'); // "it Should Be ME"
echo tukar_besar_kecil('001-A-3-5TrdYW'); // "001-a-3-5tRDyw"

?>
</body>
</html>